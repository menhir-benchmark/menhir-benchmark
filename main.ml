type parser_backend =
  | Lex_only
  | Yacc
  | Menhir_table
  | Menhir_code
  | Menhir_fred
  | Menhir_stepwise
  | Compilerlibs

module Make(Parser : Signatures.Parser) = struct
  module Parse = Parser
  module Lex = Lexer0.Make(Parser)
  let lex lexbuf =
    try
      while true do
        if Lex.token lexbuf = Signatures.Tokens.EOF
        then raise Exit
      done
    with _ -> ()
  let parse_impl lexbuf =
    Parse.implementation Lex.token lexbuf
  let parse_intf lexbuf =
    Parse.interface Lex.token lexbuf
end

module Yacc = Make(Parser0_yacc)
module Menhir_table = Make(Parser0_menhir_table)
module Menhir_code = Make(Parser0_menhir_code)
module Menhir_fred = Make(Parser0_menhir_fred)

let stepwise lexbuf =
  let module Parse = Parser0_menhir_fred in
  let token lexbuf =
    let tok = Menhir_fred.Lex.token lexbuf in
    let start = lexbuf.Lexing.lex_start_p in
    let curr = lexbuf.Lexing.lex_curr_p in
    (start, tok, curr) in
  let rec loop parser =
    match Parse.step parser with
      | `Step parser -> loop parser
      | `Feed parser ->
         loop (Parse.feed parser (token lexbuf))
      | `Reject -> failwith "reject !"
      | `Accept _ -> ()
  in
  loop (Parse.initial Parse.implementation_state (token lexbuf))

let parse backend lexbuf =
  match backend with
    | Lex_only -> Yacc.lex lexbuf
    | Yacc -> Yacc.parse_impl lexbuf
    | Menhir_table -> Menhir_table.parse_impl lexbuf
    | Menhir_code -> Menhir_code.parse_impl lexbuf
    | Menhir_fred -> Menhir_fred.parse_impl lexbuf
    | Menhir_stepwise -> stepwise lexbuf
    | Compilerlibs -> ignore (Parse.implementation lexbuf)

let backends = [
    ("lex-only", Lex_only, "only lex the files");
    ("yacc", Yacc, "ocamlyacc");
    ("table", Menhir_table, "menhir --table");
    ("code", Menhir_code, "menhir --code");
    ("compiler-libs", Compilerlibs, "compilerlibs parser (builds the AST)");
    ("fred", Menhir_fred, "menhir + Frédéric patches");
    ("stepwise", Menhir_stepwise, "menhir + Frédéric patches + stepwise loop");
  ]

let name_of_backend =
  let assoc_table =
    List.map (fun (name, variant, _doc) -> (variant, name)) backends in
  fun back -> List.assoc back assoc_table

let test backends files =
  let test_backend backend =
    let start = Unix.gettimeofday () in
    let count = ref 0 in
    files |> List.iter (fun file ->
      let ic = open_in file in
      Location.input_name := file;
      let lexbuf = Lexing.from_channel ic in
      begin
        try ignore (parse backend lexbuf)
        with _ ->
          Printf.eprintf "%S fails with backend %s\n"
            file (name_of_backend backend);
      end;
      close_in ic
    )
  in
  backends |> List.iter (fun backend ->
    let start = Unix.gettimeofday () in
    test_backend backend;
    let stop = Unix.gettimeofday () in
    Printf.printf "%s: %f\n" (name_of_backend backend) (stop -. start);
  )

let () =
  let chosen_backends = ref [] in
  let choose_backend back =
    if not (List.mem back !chosen_backends) then begin
      chosen_backends := back :: !chosen_backends;
    end
  in
  let spec =
    let backend_option (name, backend, doc) =
      ("-" ^ name, Arg.Unit (fun () -> choose_backend backend), " " ^ doc) in
    List.map backend_option backends |> Arg.align in
  let chosen_files = ref [] in
  let choose_file file = chosen_files := file :: !chosen_files in
  Arg.parse spec choose_file "";
  test !chosen_backends !chosen_files
