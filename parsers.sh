MENHIR=menhir
YACCOUT=parser0_yacc
CODEOUT=parser0_menhir_code
TABLEOUT=parser0_menhir_table

case $1 in
    ""|help)
            echo "compile targets run"
            ;;
    compile)
        echo $YACCOUT
        time ocamlyacc -b $YACCOUT parser0.mly
        time ocamlopt -c $YACCOUT.ml

        echo $CODEOUT
        time $MENHIR -b $CODEOUT parser0.mly
        time ocamlopt -c $CODEOUT.ml

        echo $TABLEOUT
        time $MENHIR --table -b $TABLEOUT parser0.mly
        time ocamlopt -c $TABLEOUT.ml
        ;;
esac
