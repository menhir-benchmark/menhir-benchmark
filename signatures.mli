(* Because parser generators define tokens in different order
   (sorted alphabetic vs. .mly order), we cannot leave them unattended
   as they would return incompatible OCaml types -- variants cannot be
   reordered.

   Instead we use the --external-tokens feature of the Menhir generator to
   ask it to reuse ocamlyacc's token definition. The Tokens module below
   is an alias to "the module that defines tokens".
 *)
module Tokens = Parser0_yacc
type token = Tokens.token

module type Parser = sig
    val implementation :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val interface :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val toplevel_phrase :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val use_file :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val parse_core_type :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val parse_expression :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
    val parse_pattern :
      (Lexing.lexbuf  -> token) -> Lexing.lexbuf -> unit
end
