# the values in Makefile.config should be tweaked to your particular install
include Makefile.config

# because we cannot combine parsers linked with different versions of
# MenhirLib, we shall use Fred's experimental branch for all parsers.
MENHIR=$(FRED_MENHIR)

##to revert to upstream menhir, just use
#FRED_LIBDIR=`ocamlfind query menhirLib`
#MENHIR=menhir

GRAMMAR=parser0.mly
YACCOUT=parser0_yacc
CODEOUT=parser0_menhir_code
TABLEOUT=parser0_menhir_table
FREDOUT=parser0_menhir_fred

PARSERS=$(YACCOUT).cmx $(TABLEOUT).cmx $(CODEOUT).cmx $(FREDOUT).cmx
PRODS=$(IMPORTED:.mli=.cmx) $(PARSERS) lexer0.cmx

# We want to functorize the lexer over the parser (to properly
# duplicate mutable lexer state). This means we need to describe the
# common signature of all parsers, most importantly the type of tokens
# (which the lexer directly depends on).

# So parsers must use compatible token types. Because ocamlyacc and
# menhir list the tokens in different order, just copying either's
# definition in the module signature does not work (variants cannot
# be reordered). So what we use instead of the --external-tokens
# feature of menhir that allows to specific an external module M whose
# type M.token is to be reused instead of re-generating a definition
# of tokens.

# This means that the YACCOUT grammar has a special status: it
# contains the definition of the token type, and in particular
# YACCOUT.cmi must be compiled before:
#
# - signatures.mli, which is the common module signature of parsers
# (whose `token` type is just `YACCOUT.token`)
#
# - all other generated parsers
#
# For this reason we list YACCOUT.cmi in the list of needed CMIS,
# before signatures.cmi.
CMIS=$(YACCOUT).cmi signatures.cmi

# --external-tokens expects a module name, so we capitalize the filename
EXTTOKENS=--external-tokens $(subst parser,Parser,$(YACCOUT))

.PHONY: all

all: $(CMIS) $(PRODS)
	ocamlfind ocamlopt -package unix -package compiler-libs -c main.ml
	ocamlfind ocamlopt \
	   -package bytes -package unix -linkpkg \
	   -package compiler-libs ocamlcommon.cmxa \
	   -I $(FRED_LIBDIR) menhirLib.cmx \
	   $(PRODS)  main.cmx -o main

$(YACCOUT).ml: $(GRAMMAR)
	ocamlyacc -b $(YACCOUT) $(GRAMMAR)

$(YACCOUT).cmi: $(YACCOUT).ml $(YACCOUT).cmx

$(CODEOUT).ml: $(CMIS) $(GRAMMAR) $(YACCOUT).cmi
	$(MENHIR) $(EXTTOKENS) -b $(CODEOUT) $(GRAMMAR)

$(TABLEOUT).ml: $(CMIS) $(GRAMMAR)
	$(MENHIR) $(EXTTOKENS) --table -b $(TABLEOUT) $(GRAMMAR)

$(FREDOUT).ml: $(CMIS) $(GRAMMAR)
	$(FRED_MENHIR) $(EXTTOKENS) --table --stepwise -b $(FREDOUT) $(GRAMMAR)

lexer0.ml: lexer0.mll $(CMIS)
	ocamllex lexer0.mll

.SUFFIXES: .ml .cmx

.ml.cmx:
	ocamlfind ocamlopt -package compiler-libs -c $(@:.cmx=.mli)
	time ocamlfind ocamlopt -package compiler-libs -I $(FRED_LIBDIR) menhirLib.cmx -c $(@:.cmx=.ml)

$(FREDOUT).cmx: $(FREDOUT).ml
	ocamlfind ocamlopt -package compiler-libs -I $(FRED_LIBDIR) -c $(FREDOUT).mli
	time ocamlfind ocamlopt -package compiler-libs -I $(FRED_LIBDIR) menhirLib.cmx -c $(FREDOUT).ml

lexer0.cmx: lexer0.ml $(CMIS)
	ocamlfind ocamlopt -package bytes -package compiler-libs -c lexer0.ml

clean:
	rm -f lexer0.ml $(PARSERS:.cmx=.ml)
	rm -f lexer0.mli $(PARSERS:.cmx=.mli)

signatures.cmi: $(YACCOUT).cmi signatures.mli
	ocamlfind ocamlc -package compiler-libs -c $(@:.cmi=.mli)
